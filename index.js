const users = `{
"id": 1,
"firstName": "Test",
"lastName": "Sample",
"email": "testsample@mail.com",
"password": "testsample",
"isAdmin": false,
"mobileNo": "0910 000 0000"
}`;

const orders = `{
"id": 1,
"userId": 1,
"productId" : 10,
"transactionDate": "11-24-2022",
"status": "paid",
"total": 540
}`;

const products = `{
"id": 10,
"name": "Mouse",
"description": "Input Device for your PC",
"price": 540,
"stock": 43,
"isActive": true
}`;

console.log(JSON.parse(users));
console.log(JSON.parse(orders));
console.log(JSON.parse(products));
